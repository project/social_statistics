function update_social_stat(module) {
  $.post(
    "/social_statistics/block/" + module,
    { url: ss_url },
    function(data) {
      $("#" + module).find("div.stat").fadeOut(1000, function() {
        $("#" + module).find("div.stat").html(data).fadeIn(1000);
      });
    }
  );
}
