<?php

define('MAGNOLIA_API_URL', 'http://ma.gnolia.com/api/rest/1/');

/*
 *  Implementation of hook_requirements
 *    Make sure the Magnolia API key is set.
 */
function ss_magnolia_requirements($phase) {
  $requirements = array();
  
  if($phase == 'runtime') {
    $t = get_t(); 
  
    $key = ss_magnolia_get_key();
    $message = empty($key) ? $t('Magnolia API Key not set!') : $key;
  
    $requirements['social_statistics_magnolia'] = array(
      'title' => $t('Magnolia Social Stats Extension'),
      'value' => ($phase == 'runtime') ? l($message, 'admin/settings/social-statistics') : $message,
    );
    
    if (empty($key)) {
      $requirements['social_statistics_magnolia']['description'] = $t('Your Social Statistics extension for Magnolia needs a key provided. Please obtain this key from Ma.gnolia.com.');
      $requirements['social_statistics_magnolia']['severity'] = REQUIREMENT_ERROR; 
    }
  }
  
  return $requirements;
}


/*
 *  Magnolia implementation of social stats API
 */
function ss_magnolia_social_stats_api($op = 'view', $url = NULL) {
  switch($op) {
    case 'view':
      //Sanity check that the URL is present
      if(is_null($url)) return FALSE;
      
      //Try to get the stats out of cache (against an MD5 of the current URL)
      $result = cache_get('ss_magnolia_' . md5($url));
      if($result) {
        //Cached - create a magnolia stat entry for the cached data
        $result = unserialize($result->data);
        $bookmarks = count($result);
        $content = theme('ss_magnolia', $bookmarks, $url, drupal_get_title());
      } else {
        //Not cached, create a template entry and add a jQuery AHAH call to get the data after page load to stop page blocking.
        drupal_add_js ('if (Drupal.jsEnabled) $(document).ready(function () { update_social_stat("ss_magnolia"); });', 'inline');
        $content = theme('ss_magnolia', theme('social_statistics_loading'), $url, drupal_get_title());
      }
      return "<div class=\"social_stat\" id=\"ss_magnolia\">{$content}</div>\n";

    case 'count' :
      //Sanity check that the URL is present
      if(is_null($url)) return FALSE;

      //Count op, used by the AHAH callback to get JUST THE NUMBER of, in this case, bookmarks
      $result = _ss_magnolia_get_data($url);
      return count($result);

    case 'settings' :
      $settings = variable_get('social_statistics', array());
      
      $form['magnolia_api_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Ma.gnolia.com API Key'),
        '#default_value' => ss_magnolia_get_key(),
        '#required' => TRUE
      );
      return $form;
  }
}




/*
 *  Get the Magnolia API Key
 */
function ss_magnolia_get_key() {
  static $key = NULL;
  
  if(is_null($key)) {
    $settings = variable_get('social_statistics', array());
    $key = isset($settings['magnolia_api_key']) ? $settings['magnolia_api_key'] : '';
  }
  
  return $key;
}




/*
 *  Magnolia Theme entry
 */
function theme_ss_magnolia($bookmarks, $url, $title = NULL) {
  //Delicious Query Arguements.
  $query_args = array('url' => $url);
  
  //If the title is set - add that as a query arg.
  if(!empty($title)) {
    $query_args['title'] = $title;
  }
  
  //Turn each key/value pair into a query line, urlencoding the key and the value.
  $query_array = array();
  foreach($query_args as $key => $value) {
    $query_array[] = urlencode($key) . '=' . urlencode($value);
  }
  
  //Implode the query array with an ampersand.
  $query = implode('&', $query_array);
  
  
  //Generate the output.
  $output = theme(
    'social_statistic',
    theme('image', drupal_get_path('module', 'social_statistics') . '/contrib/magnolia-16px.png', 'Ma.gnolia'),
    l('Ma.gnolia', 'http://ma.gnolia.com/bookmarklet/add', array('target' => '_blank'), $query),
    $bookmarks);
    
  return $output;
}



/*
 *  Private function to get the data for the current URL from Magnolia.
 *    NOTE - YOU NEED YOUR OWN KEY FOR THIS!
 */
function _ss_magnolia_get_data($url) {
  $data = 'api_key=' . ss_magnolia_get_key() . '&url=' . urlencode($url);
  $result = drupal_http_request(MAGNOLIA_API_URL . 'bookmarks_find', array(), 'POST', $data);

  if ($result->code != 200) {
    return FALSE;
  }

  $xml_parser = xml_parser_create();
  xml_set_element_handler($xml_parser, "_ss_magnolia_start_element", "_ss_magnolia_end_element");
  if (!xml_parse($xml_parser, $result->data)) {
    watchdog("warning", sprintf("XML error: %s at line %d", 
      xml_error_string(xml_get_error_code($xml_parser)), 
      xml_get_current_line_number($xml_parser)));
  }
  xml_parser_free($xml_parser);  

  
  
  // Message must be OK
  $mag_data = _ss_magnolia_get_items();
  cache_set('ss_magnolia_' . md5($url), 'cache', serialize($mag_data), CACHE_TEMPORARY);
  return $mag_data; 
}



/*
 *  XML Callback for new Magnolia XML Elements
 */
function _ss_magnolia_start_element($parser, $name, $attrs) {
  //Error response...
  if($name == 'RESPONSE' && $attrs['STATUS'] != 'ok') {
    watchdog('Social Statsitics', 'Magnolia Error - ' . $attrs['STATUS'], WATCHDOG_ERROR);
  }
  
  //Error message
  else if($name == 'ERROR') {
    watchdog('Social Statsitics', 'Magnolia Error (Error: ' . $attrs['CODE'] . ') : ' . $attrs['MESSAGE'], WATCHDOG_ERROR);
  }
  
  //Bookmark
  else if($name == 'BOOKMARK') {
    _ss_magnolia_add_item($attrs);
  }
}

/*
 *  XML Callback for closing elements - only needed to keep the PHP function above sane.
 */
function _ss_magnolia_end_element($parser, $name) { }




/*
 *  Static result caching function for the XML callback to use.
 */
function _ss_magnolia_add_item($item = NULL) {
  static $data = array();

  if(!is_null($item)) {
    $data[$item['ID']] = $item;
  }
  
  return $data;
}

/*
 *  Calls the add item function with no args and just returns the data. Human friendly wrapper really..
 */
function _ss_magnolia_get_items() {
  return _ss_magnolia_add_item();
}
